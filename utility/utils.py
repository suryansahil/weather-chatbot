from datetime import datetime, timedelta
import pandas as pd
import matplotlib.pyplot as plt
import altair as alt

def get_target_day(query):
    """
    Extracts the target day from the query.

    Args:
        query (str): The user's query.

    Returns:
        datetime: The extracted target day.
    """
    days_of_week = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
    today = datetime.now()
    target_day = None

    if "today" in query:
        target_day = today
    elif "tomorrow" in query:
        target_day = today + timedelta(days=1)
        if "day after" in query[:query.index("tomorrow")]:
            target_day+=timedelta(days=1)
    else:
        for token in query.split():
            if token in days_of_week:
                target_day = today + timedelta((days_of_week.index(token) - today.weekday()) % 7)
                break

    if not target_day:
        target_day = today
    return target_day

def get_target_hour(doc):
    """
    Extracts the target hour from the query.

    Args:
        doc (spacy.tokens.Doc): The processed spaCy document.

    Returns:
        int: The extracted target hour.
    """
    target_time = datetime.now().hour
    for token in doc:
        if token.like_num:
            if int(token.text) <= 12:  # handling both 12-hour and 24-hour formats
                target_time = int(token.text)
                for next_token in doc:
                    if next_token.text in ["am", "pm"]:
                        if next_token.text == "pm" and target_time != 12:
                            target_time += 12
                        elif next_token.text == "am" and target_time == 12:
                            target_time = 0
                        break
            elif int(token.text) <= 24:
                target_time = int(token.text)
        elif token.text in ["morning", "afternoon", "evening", "night"]:
            if token.text == "morning":
                target_time = 9
            elif token.text == "afternoon":
                target_time = 15
            elif token.text == "evening":
                target_time = 18
            elif token.text == "night":
                target_time = 21
    # if not target_time:
    #     target_hour=0
    # else:
    target_hour=target_time//3
    return target_hour

def get_unit(feature):
    """
    Return the unit for a feature query.

    Args:
        feature: The name of the weather feature.

    Returns:
        str: The relevant unit.
    """
    feature_mapping= {'cloud_cover':"%","wind_speed":"km/h","precipitation":"mm","humidity":"%"}
    feature_unit=''
    if feature in feature_mapping:
        feature_unit = feature_mapping[feature]
    if "chances_of" in feature:
        feature_unit = "%"
    return feature_unit

def construct_response(response):
    """
    Constructs the response with the requested weather features.

    Args:
        weather_data (dict): The weather data.
        target_day (datetime): The target day for the weather forecast.
        target_hour (int): The target hour for the weather forecast.
        requested_features (list): The list of requested weather features.
        temperature_unit (str): The temperature unit (Celsius or Fahrenheit).

    Returns:
        dict: The constructed response containing the requested weather features.
    """
    response_text = ''
    for key, value in response.items():
        unit=get_unit(key)
        
        if isinstance(value, str):
            value=value.replace('_', ' ')
        if "chances" in key:
            key=key.replace('_', ' ').capitalize()
            if value<30:
                response_text += f"**{key}:** {value} {unit}, Very less chances of {' '.join(key.split(' ')[2:])}  \n"
            elif value<50:
                response_text += f"**{key}:** {value} {unit}, Less chances of {' '.join(key.split(' ')[2:])}  \n"
            elif value<75:
                response_text += f"**{key}:** {value} {unit}, Chances of {' '.join(key.split(' ')[2:])}  \n"
            else:
                response_text += f"**{key}:** {value} {unit}, High chances of {' '.join(key.split(' ')[2:])}  \n"
        else:
            response_text += f"**{key.replace('_', ' ').capitalize()}:** {value} {unit}  \n"
    return response_text.strip()



def plot_other_trend(selected_trend,weather):
    """
    Plots weather trends.

    Args:
        weather: The weather data.
        selected_trend: Feature to be plotted.

    Returns:
        matplotlib.pyplot.subplots: The plotted weather trend chart.
    """
    feature = get_trend_name(selected_trend)
    trend_data=[forecast[feature] for forecast in weather["daily_forecasts"]]
    dates = [forecast["date"] for forecast in weather["daily_forecasts"]]
    df = pd.DataFrame({"Date": dates, selected_trend: trend_data})
    df["Date"] = pd.to_datetime(df["Date"]).dt.strftime('%d-%m')
    fig, ax = plt.subplots()
    ax.plot(df["Date"], df[selected_trend], label=selected_trend)
    ax.set_xlabel("Date")
    ax.set_ylabel(selected_trend)
    ax.set_title(f"{selected_trend} Trends")
    ax.legend()
    return fig

def plot_temperature(unit,weather):
    """
    Plots temperature trends.

    Args:
        weather: The weather data.
        unit (str): The unit for temperature.

    Returns:
        alt.Chart: The plotted weather trend chart.
    """
    dates = [forecast["date"] for forecast in weather["daily_forecasts"]]
    temp_c = [int(forecast["avg_temp"]) for forecast in weather["daily_forecasts"]]
    temp_f = [(int(forecast["avg_temp"])* 9/5)+32 for forecast in weather["daily_forecasts"]]

    df = pd.DataFrame({"Date": dates, "Temperature (C)": temp_c,"Temperature (F)": temp_f})
    df["Date"] = pd.to_datetime(df["Date"]).dt.strftime('%d-%m')
    fig, ax = plt.subplots()
    if unit == "Celsius":
        ax.plot(df["Date"], df["Temperature (C)"], label="Temperature (C)")
        ax.set_ylabel("Temperature (°C)")
    else:
        ax.plot(df["Date"], df["Temperature (F)"], label="Temperature (F)")
        ax.set_ylabel("Temperature (°F)")
    
    ax.set_xlabel("Date")
    ax.set_title("Temperature Trends")
    ax.legend()
    return fig


def plot_weather(df, hourly_trend, unit='C'):
    """
    Plots other weather trends.

    Args:
        df (pandas.DataFrame): The weather data.
        selected_trend (str): The selected trend to plot.
        unit (str): The unit for temperature.

    Returns:
        alt.Chart: The plotted weather trend chart.
    """
    feature=get_trend_name(hourly_trend)
    if feature in ['temperature', 'feels_like', 'max_temp', 'min_temp', 'avg_temp']:
        if unit == 'F':
            df[feature] = df[feature] * 9/5 + 32
            y_label = f'{feature} (°F)'
        else:
            y_label = f'{feature} (°C)'
    else:
        y_label = hourly_trend
    chart = alt.Chart(df).mark_line().encode(
            x=alt.X('time', title='Time'),
            y=alt.Y(feature, title=y_label),
            tooltip=[alt.Tooltip('time', title='Time'), alt.Tooltip(feature, title=y_label)]
        ).properties(
    title=f'Hourly Trend of {hourly_trend}').interactive()
    return chart

def get_trend_name(trend):
    """
    Return weather feature name.

    Args:
        trend: Trend name from user.
    Returns:
        str: Relevant feature name for.
    """
    trend_mapping = {
    "Humidity (%)": "humidity",
    "Precipitation (mm)": "precipitation",
    "Dew Point": "dew_point",
    "Cloud Cover (%)": "cloud_cover",
    "Pressure": "pressure",
    "Wind Speed (km/h)": "wind_speed",
    "Sunshine": "sunshine",
    "UV Index": "uv_index",
    "Heat Index °C": "heat_index",
    "temperature":"temperature",
    "UV Index":"uv_index",
    "Snowfall":"snowfall",
    "Moon Illumination":"moon_illumination",
    "Highest Temperature":"highest_temperature",
    "Lowest Temperature":"lowest_temperature"
}
    
    trend_options_daily = ["UV Index","Snowfall","Moon Illumination","Highest Temperature","Lowest Temperature"]
                
    if trend in trend_mapping:
        return trend_mapping[trend]

def get_hourly_data(daily_forecasts):
    """
    Return hourly data.

    Args:
        daily_forecasts: Daily forecast data from weather.
    Returns:
        dataframe: Hourly data of weather.
    """
    hourly_data = []
    for day in daily_forecasts:
        for hour in day['hourly_forecasts']:
            hour['date'] = day['date']
            hour['time'] = f"{day['date']} {hour['time']}"
            hourly_data.append(hour)
    df_hourly = pd.DataFrame(hourly_data)
    df_hourly['time'] = pd.to_datetime(df_hourly['time'], format='%Y-%m-%d %H:%M:%S')
    return df_hourly