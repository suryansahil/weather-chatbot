import pytest
from src.weather_fetch import get_weather
from utility.utils import construct_response
import asyncio
from src.nlp_processing import process_query
@pytest.fixture
def weather_data():
    data = asyncio.run(get_weather("New York"))
    return data

def test_process_query(weather_data):
    response=process_query("What is the temperature today", weather_data,'Celsius')
    assert "temperature" in response.lower()

def test_get_weather(weather_data):
    assert "location" in weather_data
    assert "datetime" in weather_data

def test_sample_queries(weather_data):
    queries = [
        "What's the weather like today?",
        "What's the weather like tomorrow?",
        "What's the weather like on Sunday?",
        "What's the current temperature?",
        "What's the low temperature for today?",
        "Is it going to rain today?",
        "Will it snow tomorrow?",
        "Is it sunny right now?",
        "What's the weather expected to be at 3 PM?",
        "How's the weather tonight?",
        "What's the weather going to be like tomorrow morning?",
        "What's the high temperature today?",
        "What's the low temperature tomorrow?",
        "What's the humidity level right now?",
        "Is it windy today?",
        "What's the UV index today?",
        "Is it cloudy right now?",
    ]

    for query in queries:
        response = process_query(query, weather_data)
        assert isinstance(response, str)  # Ensure response is a string
        assert len(response) > 0  # Ensure response is not empty

