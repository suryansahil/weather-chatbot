# Weather Forecast Application

## Description
This Weather Forecast Application allows users to obtain current and forecasted weather information for a specified city. Users can ask natural language queries about the weather and receive specific responses. Additionally, the application provides visual trends for various weather parameters.

## Features
- Get current and forecasted weather information for a city.
- Ask weather-related questions in natural language.
- Display visual trends for temperature, humidity, and other weather parameters.
- Interactive plots with options to toggle between Celsius and Fahrenheit.

## Installation
1. Clone the repository:
    ```bash
    git clone https://gitlab.com/suryansahil/weather-chatbot.git
    ```
2. Navigate to the project directory:
    ```bash
    cd weather-chatbot
    ```

3. Create virtual activate Environment:
    ```bash
    python -m venv {virtual_env_name}
    Linux:source {virtual_env_name}/bin/activate
    Windows: {virtual_env_name}/Scripts/activate
    ```
3. Install the required dependencies:
    ```bash
    pip install -r requirements.txt
    ```

## Usage
1. Run the Streamlit application:
    ```bash
    streamlit run app.py
    ```
2. Enter a city in the input box to fetch the weather data.
3. Ask a weather-related question in the query input box.
4. View the weather data and trends on the main page.
5. Use the sidebar to toggle temperature units and select trends to display.

## File Structure
- `app.py`: Main application file.
- `src/nlp_processing.py`: Contains functions for processing natural language queries.
- `src/weather_fetch.py`: Fetches weather data from the weather API.
- `utility/token_mapping.py`: Mapping of tokens to weather data attributes.
- `utility/utils.py`: Utility functions including constructing responses.
- `test/test_app.py`: Test file for the application.
- `requirements.txt`: Contains the list of dependencies.

## Example Queries
- "What's the weather like today?"
- "What's the current temperature?"
- "Is it going to rain today?"
- "How's the weather tomorrow night?"

## Testing
Run the tests using:
```bash
pytest test_app.py
