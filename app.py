import streamlit as st
import asyncio
from src.weather_fetch import get_weather
from src.nlp_processing import process_query
import nltk
import pandas as pd
from utility.utils import plot_other_trend,plot_temperature,plot_weather,get_hourly_data
import traceback

# Ensure NLTK data is downloaded. Uncomment for first run.
# nltk.download('punkt')
# nltk.download('averaged_perceptron_tagger')


def main():
    st.title("Weather Chat Bot")
    city_input = st.sidebar.text_input("City:")
    user_input = st.sidebar.text_input("Enter your query:")
    unit_options = ["Celsius","Fahrenheit"]
    selected_unit = st.sidebar.selectbox("Select a unit to display", unit_options)
    try:
        if user_input and city_input:
            weather = asyncio.run(get_weather(city_input))
            location =weather['location']
            st.markdown(f"Weather for **{location}**")
            
            if weather:
                response = process_query(user_input, weather,selected_unit)
                if response:
                    st.markdown(response)
                st.subheader("Weather Trends")
                fig_temperature = plot_temperature( selected_unit,weather)
                trend_options_hourly = ["Humidity (%)", "Wind Speed (km/h)", "Precipitation (mm)", "Cloud Cover (%)","Pressure","Dew Point","UV Index","Heat Index °C"]
                trend_options_daily = ["UV Index","Snowfall","Moon Illumination","Highest Temperature","Lowest Temperature"]
                daily_forecasts = weather['daily_forecasts']

            # Convert to DataFrame and handle duplicate columns
                df_daily = pd.json_normalize(daily_forecasts)
                days = df_daily['date'].unique()
                df_hourly= get_hourly_data(daily_forecasts)
                selected_day = st.sidebar.selectbox("Select a day for hourly forecast:", days)
                
                if selected_day:
                    df_hourly_selected = df_hourly[df_hourly['date'] == selected_day]
                    col1, col2 = st.columns(2)
                    with col1:
                        st.pyplot(fig_temperature, use_container_width=True)
                    with col2:
                        unit = 'F' if selected_unit == "Fahrenheit" else 'C'
                        st.altair_chart(plot_weather(df_hourly_selected, 'temperature', unit), use_container_width=True)
                    col1, col2 = st.columns(2)
                    with col1:
                        selected_trend = st.selectbox("Select a trend to display", trend_options_daily)
                        fig_other = plot_other_trend(selected_trend,weather)
                        st.pyplot(fig_other, use_container_width=True)
                    with col2:
                        hourly_trend = st.selectbox("Select a trend to display hourly forecast", trend_options_hourly)
                        st.altair_chart(plot_weather(df_hourly_selected,hourly_trend), use_container_width=True)
            else:
                st.write("Sorry, I couldn't fetch the weather for the given city.")
    except Exception as e:
        print(e)
        print(traceback.format_exc())
        st.write("Sorry, I couldn't fetch the weather for the given city. Please try again.")

if __name__ == "__main__":
    main()