import python_weather
import asyncio

async def get_weather(city):
    """
    Fetches the weather data for the given city.
    Args:
        city (str): The name of the city.
    Returns:
        dict: The fetched weather data.
    """
    client = python_weather.Client(unit=python_weather.METRIC,locale=python_weather.Locale.ENGLISH)
    weather = await client.get(city)
    await client.close()
    weather_data = extract_weather_info(weather)
    return weather_data

def extract_weather_info(weather_data):
    """
    Converts data into a lookup dictionary.
    Args:
        weather_data: Forecast object with weather data
    Returns:
        dict: The fetched weather data.
    """
    response = {
        "location": weather_data.location,
        "datetime": weather_data.datetime,
        "local_population": weather_data.local_population,
        "region": weather_data.region,
        "coordinates": weather_data.coordinates,
        "region":weather_data.region,
        "coordinates":weather_data.coordinates,
        "feels_like_°C": weather_data._Forecast__inner['current_condition'][0]['FeelsLikeC'],
        "feels_like_°F": weather_data._Forecast__inner['current_condition'][0]['FeelsLikeF'],
        "localObsDateTime": weather_data._Forecast__inner['current_condition'][0]['localObsDateTime'],
        "observation_time": weather_data._Forecast__inner['current_condition'][0]['observation_time'],
        "precipInches": weather_data._Forecast__inner['current_condition'][0]['precipInches'],
        "precipMM": weather_data._Forecast__inner['current_condition'][0]['precipMM'],
        "pressure_Inches": weather_data._Forecast__inner['current_condition'][0]['pressureInches'],
        "visibility_Miles": weather_data._Forecast__inner['current_condition'][0]['visibilityMiles'],
        "weather_Code": weather_data._Forecast__inner['current_condition'][0]['weatherCode'],
        "wind_speed_Kmph": weather_data._Forecast__inner['current_condition'][0]['windspeedKmph'],
        "wind_speed_Miles": weather_data._Forecast__inner['current_condition'][0]['windspeedMiles'] ,
        "Lat_Lon": weather_data._Forecast__inner['request'][0]['query'],
        
        "daily_forecasts": [
            {
                "date": forecast.date.strftime("%Y-%m-%d"),
                "avg_temp":forecast._DailyForecast__inner['avgtempC'],
                "avg_temp_°F":forecast._DailyForecast__inner['avgtempF'],
                "max_temp":forecast._DailyForecast__inner['maxtempC'],
                "max_temp_°F":forecast._DailyForecast__inner['maxtempF'],
                "min_temp":forecast._DailyForecast__inner['mintempC'],
                "min_temp_°F":forecast._DailyForecast__inner['mintempF'],
                "sun_hour":forecast._DailyForecast__inner['sunHour'],
                "uv_index":forecast._DailyForecast__inner['uvIndex'],
                "totalSnow_cm":forecast._DailyForecast__inner['totalSnow_cm'],
                "highest_temperature": forecast.highest_temperature,
                "lowest_temperature": forecast.lowest_temperature,
                "temperature": forecast.temperature,
                "moon_illumination": forecast.moon_illumination,
                "sunlight": forecast.sunlight,
                "snowfall": forecast.snowfall,
                "moon_phase":forecast._DailyForecast__astronomy['moon_phase'],
                "moonrise":forecast._DailyForecast__astronomy['moonrise'],
                "moonset":forecast._DailyForecast__astronomy['moonset'],
                "sunrise":forecast._DailyForecast__astronomy['sunrise'],
                "sunset":forecast._DailyForecast__astronomy['sunset'],
                "hourly_forecasts": [
                    {
                        "time": hourly.time,
                        "chances_of_fog": hourly.chances_of_fog,
                        "chances_of_frost": hourly.chances_of_frost,
                        "chances_of_high_temperature": hourly.chances_of_high_temperature,
                        "chances_of_overcast": hourly.chances_of_overcast,
                        "chances_of_rain": hourly.chances_of_rain,
                        "chances_of_remaining_dry": hourly.chances_of_remaining_dry,
                        "chances_of_snow": hourly.chances_of_snow,
                        "chances_of_sunshine": hourly.chances_of_sunshine,
                        "chances_of_thunder": hourly.chances_of_thunder,
                        "chances_of_windy": hourly.chances_of_windy,
                        "cloud_cover": hourly.cloud_cover,
                        "dew_point": hourly.dew_point,
                        "feels_like": hourly.feels_like,
                        "feels_like_°C": hourly._BaseForecast__inner['FeelsLikeC'],
                        "feels_like_°F": hourly._BaseForecast__inner['FeelsLikeF'],
                        "heat_index":hourly.heat_index.index,
                        "heat_index_state":hourly.heat_index.name,
                        "heat_index_value":hourly.heat_index.value,
                        "weatherDesc": hourly.kind.name,
                        "weatherDesc_value":hourly.kind.value,
                        "weatherDesc_emoji":hourly.kind.emoji,
                        "precipitation": hourly.precipitation,
                        "pressure": hourly.pressure,
                        "temperature": hourly.temperature,
                        "temperature_°C": hourly._BaseForecast__inner['temp_C'],
                        "temperature_°F": hourly._BaseForecast__inner['temp_F'],
                        "uv_index": hourly.ultraviolet.index,
                        "uv_state": hourly.ultraviolet.name,
                        "visibility_km": hourly.visibility,
                        "wind_chill": hourly.wind_chill,
                        "wind_direction": hourly.wind_direction.name,
                        "wind_degrees" : hourly.wind_direction.degrees,
                        "wind_emoji":hourly.wind_direction.emoji,
                        "wind_gust": hourly.wind_gust,
                        "wind_speed": hourly.wind_speed,
                        "humidity": hourly.humidity,
                    } for hourly in forecast.hourly_forecasts
                ]
            } for forecast in weather_data.daily_forecasts
        ]
    }
    return response

