import spacy
from datetime import datetime, timedelta
from utility.token_mapping import TOKEN_MAPPING
from utility.utils import construct_response,get_target_day,get_target_hour
# Load spaCy model
nlp = spacy.load('en_core_web_sm')

def process_query(query, weather_data,selected_unit='Celsius'):
    """
    Processes the natural language query to extract weather-related information.

    Args:
        query (str): The user's query.
        weather_data (dict): The weather data.
        selected_unit (str): The temperature unit (Celsius or Fahrenheit).

    Returns:
        dict: The response containing the extracted weather information.
    """
    doc = nlp(query.lower())
    target_day = get_target_day(query)
    target_hour = get_target_hour(doc)
    requested_features = []
    temperature_unit = "C"
    if selected_unit == 'Celsius':
        temperature_unit = "C"
    else:
        temperature_unit = "F"
    
    for token in doc:
        token_text = token.text
        if token_text in ["sun", "moon","max","min","avg","maximum","minimum","average","wind","high","low"]:
            if query.split().index(token_text) < len (query.split())-1:
                next_token_text = query.split()[query.split().index(token_text) + 1]
                if f"{token_text}{next_token_text}" in TOKEN_MAPPING:
                    requested_features.extend(TOKEN_MAPPING[f"{token_text}{next_token_text}"])
            elif token_text in TOKEN_MAPPING:
                requested_features.extend(TOKEN_MAPPING[token_text])
        elif token_text in TOKEN_MAPPING:
            requested_features.extend(TOKEN_MAPPING[token_text])
        if token_text in ["chance", "chances","will"]:
            for next_token in doc:
                if next_token.lemma_ in TOKEN_MAPPING['chances']:
                    requested_features.append(TOKEN_MAPPING['chances'][next_token.lemma_])
        elif token_text in ["fahrenheit", "f","F"]:
            temperature_unit = "F"
        elif token_text in ["celsius", "c","C"]:
            temperature_unit = "C"

    if not requested_features:
        requested_features.extend(["description"])

    # print(requested_features)
        

    target_day_str = target_day.strftime("%Y-%m-%d")
    relevant_forecast = None

    for forecast in weather_data["daily_forecasts"]:
        if forecast["date"] == target_day_str:
            relevant_forecast = forecast
            break
    response = {}

    if relevant_forecast:
        hourly_forecast = relevant_forecast["hourly_forecasts"][target_hour]
        for feature in requested_features:
            if feature in hourly_forecast:
                response[feature] = hourly_forecast.get(f"{feature}_°{temperature_unit}") or hourly_forecast.get(feature, "N/A")
            elif feature in relevant_forecast:
                response[feature] = relevant_forecast.get(f"{feature}_°{temperature_unit}") or relevant_forecast.get(feature, "N/A")
            elif feature in weather_data:
                response[feature] = weather_data.get(f"{feature}_°{temperature_unit}") or weather_data.get(feature, "N/A")
            elif feature == "description":
                response["description"] = hourly_forecast["weatherDesc"]

    # Check for temperature unit
    if temperature_unit == "F":
        response = {key: f"{value} °F" if "chances" not in key and ("temp" in key or "temperature" in key or 'feels_like' in key) else value for key, value in response.items()}
    else:
        response = {key: f"{value} °C" if "chances" not in key and ( "temp" in key or "temperature" in key or 'feels_like' in key) else value for key, value in response.items()}

    # Construct natural language response
    response_text = ''
    if response:
        response_text=construct_response(response)
    else:
        response_text += f"Weather: {response}  \n"

    return response_text.strip()

